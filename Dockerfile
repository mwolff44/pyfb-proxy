FROM debian:buster

MAINTAINER Mathias WOLFF <mathias@celea.org>

# Important! Update this no-op ENV variable when this Dockerfile
# is updated with the current date. It will force refresh of all
# of the base images and things like 'apt-get update' won't be using
# old cached versions when the Dockerfile is built.
ENV REFRESHED_AT 2020-05-14
ENV VERSION 3.0.0

ENV KAMAILIO_LOG_LEVEL info

# avoid httpredir errors
RUN sed -i 's/httpredir/deb/g' /etc/apt/sources.list

RUN rm -rf /var/lib/apt/lists/* && apt-get update &&   apt-get install --assume-yes gnupg wget

# kamailio repo
RUN echo "deb http://deb.kamailio.org/kamailio53 buster main" >   /etc/apt/sources.list.d/kamailio.list
RUN wget -O- http://deb.kamailio.org/kamailiodebkey.gpg | apt-key add -

RUN apt-get update && apt-get install --assume-yes \
redis-tools \
kamailio \
kamailio-extra-modules \
kamailio-json-modules \
kamailio-postgres-modules \
kamailio-redis-modules \
kamailio-tls-modules \
kamailio-utils-modules \
kamailio-xml-modules

VOLUME /etc/kamailio

# clean
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

#setup dumb-init
RUN curl -k -L https://github.com/Yelp/dumb-init/releases/download/v1.2.1/dumb-init_1.2.1_amd64 > /usr/bin/dumb-init
RUN chmod 755 /usr/bin/dumb-init

# Config files.
ADD ./src/kamailio.cfg /etc/kamailio/kamailio.cfg

ADD ./script/run.sh /run.sh
RUN chmod +x /run.sh
RUN touch /env.sh
ENTRYPOINT ["/run.sh"]
CMD ["/usr/sbin/kamailio", "-DD", "-P", "/var/run/kamailio.pid", "-f", "/etc/kamailio/kamailio.cfg"]
